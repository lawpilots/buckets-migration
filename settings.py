import os

# Telekom (Open Telekom Cloud)
OTC_ACCESS_KEY = os.getenv('OTC_ACCESS_KEY')
OTC_SECRET_KEY = os.getenv('OTC_SECRET_KEY')
OTC_SERVER_URL = os.getenv('OTC_SERVER_URL')
SOURCE_BUCKET = os.getenv('SOURCE_BUCKET')

# GCP
GOOGLE_APPLICATION_CREDENTIALS = os.getenv('GOOGLE_APPLICATION_CREDENTIALS')
TARGET_BUCKET = os.getenv('TARGET_BUCKET')

# DB
DB_USER = os.getenv('DB_USER')
DB_PASSWORD = os.getenv('DB_PASSWORD')
DB_HOST = os.getenv('DB_HOST')
DB_PORT = os.getenv('DB_PORT')
DB_NAME = os.getenv('DB_NAME')
