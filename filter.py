"""
Filter the initial list of files from the bucket so only current version + last version
of base course will be included in the list to migrate
TODO:
1. Connect to the DB
2. retrieve version_counter from company_basecourse
3. for every lang retrieve current version number
4. calculate previous version as well (if found)
5. filter the list by this
"""
from models import BaseCourse, CourseCode

import datetime
import asyncio
import nest_asyncio

nest_asyncio.apply()


class FilteredBaseCourseObjectsList:
    def __init__(self, number_of_versions_to_support: int = 1,
                 desired_lang: list = None, desired_base_courses: list = None):
        self.objects = []
        self.number_of_versions_to_support = number_of_versions_to_support
        self.desired_lang = desired_lang
        self.desired_base_courses = desired_base_courses
        self.initial_tasks = []
        self.tasks_2 = []
        self.tasks_3 = []

    @property
    def base_courses(self):
        if not self.desired_base_courses:
            yesterday = datetime.datetime.today() - datetime.timedelta(days=1)
            return CourseCode.get_active_base_courses(f'{yesterday.year}-{yesterday.month}-{yesterday.day}')
        else:
            return self.desired_base_courses

    def main(self):
        loop = asyncio.get_event_loop()
        self.populate()
        loop.run_until_complete(asyncio.gather(*self.initial_tasks))
        loop.run_until_complete(asyncio.gather(*self.tasks_2))
        loop.run_until_complete(asyncio.gather(*self.tasks_3))
        return self.objects

    def populate(self):
        """
        TODO:
        - add here all other initial dirs besides course/base_course
        :return:
        append tasks to self.initial_tasks
        """
        for base_course in self.base_courses:
            self.initial_tasks.append(self._handle_base_course(base_course_id=base_course))

    async def _handle_base_course(self, base_course_id):
        base_course = BaseCourse(base_course_id)
        relevant_versions: dict = base_course.desired_versions(self.number_of_versions_to_support, self.desired_lang)
        for lang, versions in relevant_versions.items():
            self.tasks_2.append(self._handle_versions(lang=lang, versions=versions, base_course_id=base_course_id))

    async def _handle_versions(self, lang, versions, base_course_id):
        for version in versions:
            self.tasks_3.append(self._handle_version(lang=lang, version=version, base_course_id=base_course_id))

    async def _handle_version(self, lang, version, base_course_id):
        relevant_keys = BaseCourse(base_course_id).get_relevant_keys_from_bucket(
            lang=lang, version_number=version)
        self.objects += relevant_keys



