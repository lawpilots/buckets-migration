from sqlalchemy.ext.automap import automap_base
from __init__ import db_session, engine
from otc import OTCClient

Base = automap_base()
Base.prepare(engine, reflect=True)

BaseCourseModel = Base.classes.company_basecourse
CourseCodeModel = Base.classes.company_coursecode


class BaseCourse:
    def __init__(self, base_course_id):
        self.id = base_course_id
        self.entry = db_session.query(BaseCourseModel).get(self.id)

    def desired_versions(self, number_of_versions_to_support=1, desired_lang: list = None):
        versions_dict = {}
        versions_string: str = self.entry.version_counter
        list_of_versions = versions_string.split(',')

        for version in list_of_versions:
            lang, current_version_number = version.split('=')
            if desired_lang:
                if lang not in desired_lang:
                    continue
            if int(current_version_number) > 1:
                supported_versions = []
                for i in range(number_of_versions_to_support):
                    supported_versions.append(int(current_version_number) - i)
                versions_dict[lang] = supported_versions
            elif int(current_version_number) == 1:
                versions_dict[lang] = [current_version_number]
        print(f'{self.entry.name}={versions_dict}')
        return versions_dict

    def get_relevant_keys_from_bucket(self, lang, version_number):
        otc = OTCClient()
        prefix = f'course/base_courses/{self.entry.name}/{lang}/{version_number}'
        otc.populate_objects(path=prefix, course=True)
        return otc.objects


class CourseCode:
    def __init__(self, base_course_id):
        self.id = base_course_id
        self.entry = db_session.query(BaseCourseModel).get(self.id)

    @staticmethod
    def get_active_base_courses(date):
        active_base_courses = [course_code.base_course_id for course_code in
                               db_session.query(CourseCodeModel).filter(
                                   (CourseCodeModel.next_expiration_date > date) |
                                   (CourseCodeModel.expiration_date > date)
                               ).all()]
        # make distinct
        active_base_courses = list(set(active_base_courses))
        return active_base_courses

