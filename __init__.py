from sqlalchemy import create_engine, MetaData
from sqlalchemy.orm import scoped_session, sessionmaker

from settings import (DB_USER, DB_PASSWORD, DB_HOST, DB_PORT, DB_NAME)


# dialect+driver://username:password@host:port/database
db_connect_string = f'postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}'
engine = create_engine(db_connect_string)
metadata = MetaData(engine)

db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))
