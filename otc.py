from obs import ObsClient
import tempfile
import settings


class OTCClient:
    def __init__(self):
        self.client = ObsClient(
            access_key_id=settings.OTC_ACCESS_KEY,
            secret_access_key=settings.OTC_SECRET_KEY,
            is_secure=True,
            server=settings.OTC_SERVER_URL,
            ssl_verify=False,
            port=443,
            max_retry_count=1,
            timeout=20,
            chunk_size=65536,
            long_conn_mode=True)
        self.objects = []
        self.object_counter = 0

    def list_all_buckets(self):
        resp = self.client.listBuckets()
        print(f'common msg:status: {resp.status}'
              f',errorCode: {resp.errorCode}'
              f',errorMessage: {resp.errorMessage}', flush=True)

        # list all buckets
        buckets_list = resp.body
        if buckets_list:
            print('owner_id:', buckets_list.owner.owner_id, ',owner_name:', buckets_list.owner.owner_name)
            i = 0
            for item in buckets_list.buckets:
                print(f'buckets[{i}]:')
                print(f'bucket_name: {item.name} ,create_date: {item.create_date}')
                i += 1

    def populate_objects(self, path=None, marker=None, bucket_name=settings.SOURCE_BUCKET, course=False):
        while True:
            if not course:
                resp = self.client.listObjects(bucketName=bucket_name, prefix=path, max_keys=1000, marker=marker)
                self.object_counter += len(resp.body.contents)
                objects = [content.key for content in resp.body.contents if 'base_courses' not in content.key]
                self.objects += objects

            if course:
                resp = self.client.listObjects(bucketName=bucket_name, prefix=path, max_keys=1000, marker=marker)
                self.object_counter += len(resp.body.contents)
                objects = [content.key for content in resp.body.contents]
                self.objects += objects

            if resp.body.next_marker:
                marker = resp.body.next_marker
            else:
                break

    def download_file_to_temp(self, object_key, otc_bucket_name=settings.SOURCE_BUCKET):
        temp = tempfile.NamedTemporaryFile(dir='downloads', delete=False)
        while True:
            try:
                self.client.getObject(otc_bucket_name, object_key, downloadPath=temp.name)
            except Exception as e:
                print(f'excepting: {e}', flush=True)
                continue
            finally:
                break
        return temp.name, object_key










