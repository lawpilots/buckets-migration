import settings
from otc import OTCClient
import gcp
import os
import time
import multiprocessing as mp

from filter import FilteredBaseCourseObjectsList


def handle_object(_object):
    path, object_key = OTCClient().download_file_to_temp(_object)
    object_obs = OTCClient().client.getObjectMetadata(settings.SOURCE_BUCKET, object_key)    # we get the object to get content type and other info from it
    gcp.upload_blob(path, object_key, object_obs)
    os.remove(path)


def migrate_buckets(desired_dirs=None, number_of_base_course_versions_to_support=1):
    all_objects = []
    otc_client = OTCClient()
    start = time.time()

    num_workers = mp.cpu_count()
    pool = mp.Pool(num_workers)

    # populate needed files - base courses and the rest
    # base courses files
    base_courses_files = FilteredBaseCourseObjectsList(
        number_of_versions_to_support=number_of_base_course_versions_to_support).main()

    # rest of the files
    if desired_dirs is None:
        otc_client.populate_objects()
    else:
        for _dir in desired_dirs:
            otc_client.populate_objects(path=_dir)

    # add to object list
    print(f'base_course files:{len(base_courses_files)}')
    print(f'all others:{len(otc_client.objects)}')

    all_objects += base_courses_files
    all_objects += otc_client.objects

    print(f'all objects: {len(all_objects)}')

    # now work
    for _object in all_objects:
        pool.apply_async(handle_object, args=(_object, ))

    pool.close()
    pool.join()
    end = time.time()
    print(f'migration done in {(end - start)/60} min')


if __name__ == '__main__':
    migrate_buckets(number_of_base_course_versions_to_support=2)
