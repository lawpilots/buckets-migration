# Imports the Google Cloud client library
import mimetypes


from google.cloud import storage
import settings


def upload_blob(source_file_name, destination_blob_name, obs_object, bucket_name=settings.TARGET_BUCKET):
    """Uploads a file to the bucket."""
    # bucket_name = "your-bucket-name"
    # source_file_name = "local/path/to/file"
    # destination_blob_name = "storage-object-name"

    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)

    _type, encoding = mimetypes.guess_type(destination_blob_name)

    content_type = obs_object.get('body').get('content_type') or _type or 'application/octet-stream'
    blob = bucket.blob(destination_blob_name)
    while True:
        try:
            # obsClient.getObject(otc_bucket_name, object_key, downloadPath=temp.name)
            blob.upload_from_filename(source_file_name, content_type=content_type)
        except Exception as e:
            print(f'excepting: {e}', flush=True)
            continue
        finally:
            break

    # print(
    #     "File {} uploaded to {}.".format(
    #         source_file_name, destination_blob_name
    #     )
    # )
